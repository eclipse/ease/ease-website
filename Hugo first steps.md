see: https://gohugo.io/getting-started/quick-start/

    --> checkout EASE website
    hugo new site <checkout dir> --force
    cd <checkout dir>
    git submodule add https://gitlab.eclipse.org/eclipsefdn/it/webdev/hugo-solstice-theme.git themes/solstice
    echo "theme = 'solstice'" >> config.toml
    hugo server
    hugo server -D  // for draft pages

create new pages:

    cd <checkout dir>
    hugo new folder/page.md

get fontawesome icons from

	https://fontawesome.com/v4/icons/
	
	use: {{% fa fa-download %}}
	
Hugo functions:

	https://gohugo.io/functions/
	https://janert.me/guides/hugo-survival-guide/
	
Change website:

  push your changes to main (w/o separate branch & pull request)
  run jenkins job: https://ci.eclipse.org/ease/job/ease.build.website.manual/