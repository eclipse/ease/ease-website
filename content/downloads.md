---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---
## {{% fa icon-download-alt %}} Downloads

### Releases

{{% indent %}}{{% fa fa-download %}} Latest: <span class="p2site" onclick="javascript:prompt('Hit Ctrl-c to copy', 'https://download.eclipse.org/ease/release/latest');">https://download.eclipse.org/ease/release/latest</span>

{{% indent %}}{{% fa fa-link %}} Dedicated releases: [https://download.eclipse.org/ease/release](https://download.eclipse.org/ease/release)

{{% indent %}}{{% fa fa-link %}} Offline zips: [https://download.eclipse.org/ease/release/offline](https://download.eclipse.org/ease/release/offline)

### Integration builds

{{% indent %}}{{% fa fa-download %}} Nightly: <span class="p2site" onclick="javascript:prompt('Hit Ctrl-c to copy', 'https://download.eclipse.org/ease/integration/nightly');">https://download.eclipse.org/ease/integration/nightly</span>

### EASE Module Doclet

{{% indent %}}{{% fa fa-download %}} Nightly: [ease.module.doclet.jar](https://ci.eclipse.org/ease/job/ease.build.nightly/lastSuccessfulBuild/artifact/ease.module.doclet.jar)

---

### Compatibility

Compatibility checks will pick up the latest nightly build and run its JUnit tests against an older target platform. This guarantees, that EASE can be installed on an older Eclipse version and that its basic features are working properly. As some EASE features (mostly UI) do require newer versions of Eclipse, you might experience feature degradation on older platforms.

The matrix below is not complete. In case you do use something we do not track, please [report to the mailing list](https://dev.eclipse.org/mailman/listinfo/ease-dev), so we can update the matrix for other users.


<table id="compatibility">
		<tr>
			<th><sub>Target</sub>/<sup>EASE</sup></th>
			<th>Nightly</th>
			<th>0.9.0</th>
			<th>0.8.0</th>
			<th>0.7.0</th>
			<th>0.6.0</th>
			<th>0.5.0</th>
			<th>0.4.0</th>
			<th>0.3.0</th>
			<th>0.2.0</th>
			<th>0.1.0</th>
		</tr>
{{% compatibility "2023-12" T - - - - - - - - - %}}
{{% compatibility "2023-09" T - - - - - - - - - %}}
{{% compatibility "2023-06" T - - - - - - - - - %}}
{{% compatibility "2023-03" T - - - - - - - - - %}}
{{% compatibility "2022-12" T - - - - - - - - - %}}
{{% compatibility "2022-09" T - - - - - - - - - %}}
{{% compatibility "2022-06" T R - - - - - - - - %}}
{{% compatibility "2022-03" T O - - - - - - - - %}}
{{% compatibility "2021-12" T O - - - - - - - - %}}
{{% compatibility "2021-09" T O - - - - - - - - %}}
{{% compatibility "2021-06" T O O - - - - - - - %}}
<tr><th>2021-03</th><td colspan=10>Target platform 2021-03 is broken for EASE, see <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=572102">bug 572102</a></td></tr>
{{% compatibility "2020-12" T O O - - - - - - - %}}
{{% compatibility "2020-09" T O O - - - - - - - %}}
{{% compatibility "2020-06" T O O - - - - - - - %}}
{{% compatibility "2020-03" T O O - - - - - - - %}}
{{% compatibility "2019-12" T O O - - - - - - - %}}
{{% compatibility "2019-09" T O O - - - - - - - %}}
{{% compatibility "2019-06" T O R R - - - - - - %}}
{{% compatibility "2019-03" T O O R - - - - - - %}}
{{% compatibility "2018-12" T O O O - - - - - - %}}
{{% compatibility "2018-09" X O O O - - - - - - %}}
{{% compatibility "Photon"  X O O R - - - - - - %}}
{{% compatibility "Oxygen"  X O O O R R - - - - %}}
{{% compatibility "Neon"    X X X O - - R - - - %}}
{{% compatibility "Mars"    X X X X - - - R - - %}}
{{% compatibility "Luna"    X X X X - - - - R R %}}
</table>
