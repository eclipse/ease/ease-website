---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---
## {{% fa icon-cogs %}} Support

*"I need some guidance and tutorials"*

{{% indent %}} {{% fa fa-book %}} {{% indent 1 %}} [Documentation](../documentation)

*"I have a specific question regarding usage"*

{{% indent %}}{{% fa fa-list %}} {{% indent 1 %}} [EASE Forums](https://www.eclipse.org/forums/index.php/f/292/)

{{% indent %}}{{% fa fa-stack-overflow %}} {{% indent 1 %}} [Stackoverflow](https://stackoverflow.com/questions/tagged/ease) - Browse relevant topics.

{{% indent %}}{{% fa fa-stack-overflow %}} {{% indent 1 %}} [Stackoverflow](https://stackoverflow.com/questions/ask?tags=ease) - Ask technical questions using tag ease.

*"I am a developer and have a specific question regarding implementation/API"*

{{% indent %}}{{% fa fa-envelope %}} {{% indent 1 %}} [Developer Mailing List](https://dev.eclipse.org/mailman/listinfo/ease-dev) - Ask technical questions on our mailing list.

*"I found a bug / want to request a feature"*

{{% indent %}}{{% fa fa-bug %}} {{% indent 1 %}} [File a bug](https://gitlab.eclipse.org/eclipse/ease/ease/-/issues/new) - file bugs and feature requests

{{% indent %}}{{% fa fa-bug %}} {{% indent 1 %}} [Gitlab issues](https://gitlab.eclipse.org/groups/eclipse/ease/-/issues/?sort=created_date&state=opened) - review open topics
