---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---
## {{% fa icon-comments-alt %}} Developers

Source code is hosted on [gitlab](https://gitlab.eclipse.org/eclipse/ease):

{{% indent %}} {{% fa fa-gitlab %}} {{% indent 1 %}} [EASE](https://gitlab.eclipse.org/eclipse/ease/ease) - Script framework, engines and modules

{{% indent %}} {{% fa fa-gitlab %}} {{% indent 1 %}} [EASE Scripts](https://gitlab.eclipse.org/eclipse/ease/ease-scripts) - Useful scripts & tutorials

{{% indent %}} {{% fa fa-gitlab %}} {{% indent 1 %}} [EASE Website](https://gitlab.eclipse.org/eclipse/ease/ease-website) - This web presence content

Some additional components are hosted on [github](https://github.com/eclipse-ease-addons):

{{% indent %}} {{% fa fa-github %}} {{% indent 1 %}} [eclipse-ease-addons/groovy](https://github.com/eclipse-ease-addons/groovy) - Groovy interpreter

{{% indent %}} {{% fa fa-github %}} {{% indent 1 %}} [eclipse-ease-addons/jruby](https://github.com/eclipse-ease-addons/jruby) - JRuby interpreter

---

Please read the [Contributor Guide](http://wiki.eclipse.org/EASE/Contributor_Guide) for detailed information.

Developers should consider subscribing to the [ease-dev mailing list](https://dev.eclipse.org/mailman/listinfo/ease-dev).

---

Big thanks to YourKit which supports open source developers with a free license for [YourKit Java Profiler](https://www.yourkit.com/java/profiler/). If you work on EASE topics you may apply for a free license by sending an email to [sales@yourkit.com](mail:sales@yourkit.com).

![YourKit](https://www.yourkit.com/images/yklogo.png)
