---
title: "Eclipse Advanced Scripting Environment"
date: 2020-03-01T16:09:45-04:00

hide_breadcrumb: true
headline: "EASE Scripting"
tagline: "Make scripting the IDE fun"
hide_page_title: true

layout: "single"
page_css_file: "/css/ease.css"
---

## Eclipse Advanced Scripting Environment (EASE)


### EASE is a scripting environment for Eclipse.

It allows to create, maintain and execute script code in the context of the running Eclipse instance. Such scripts are interpreted using the current Java runtime. Therefore they have access to Java classes and the IDE itself.

![Scripting in action](images/workspace.gif)

---

### Libraries support

All provided engines allow usage of modules: libraries written in Java and made easily accessible from script code. Like script engines, modules can be added to the framework quite easily.

![Script Libraries](images/modules.gif)

---

### Extending the IDE

When script storage locations (like the workspace, the file system or even a webserver) are registered in preferences, EASE can integrate scripts seamlessly in the UI:

![Scripts](images/script.gif)

---

### Language Support

[Various script engines are available](documentation/supported_engines), like

- JavaScript
- Python
- Groovy

Further languages allow to encapsulate scripts in archive files, run regression tests or even compile Java code dynamically.

The extensible framework allows to [add any kind of language](http://codeandme.blogspot.com/p/blog-page_47.html), you could even embed your own command shell.
