---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---

## Registered Workspace Scripts

Scripts meant to be used on a regular basis can be registered, so EASE knows about them and tracks them. 
![Register Scripts](../../images/register_scripts.png)

You may store your scripts in the workspace, somewhere on the file system or even on a webserver providing [directory index](https://en.wikipedia.org/wiki/Web_server_directory_index) support.

The __Scripts Explorer__ view allows to easily access all registered scripts, to execute and edit them (for editable locations).

EASE scans scripts for comment header that contain certain _key : value_ pairs. When a value is recognized, certain behaviors can be triggered. For scripts that cannot be modified, you may edit values by using the __Properties__ view.

### Known Keywords

| Key | Type | Description |
|:---:|:---:|:---:|
| name             | Display | Script name displayed. May use '/' to create folder structures. Defaults to the filename.
| image            | Display | Add a dedicated toolbar/menu image. Accepts local file paths, relative paths (according to the script file) or URIs. Accepted URI schemes are http://, platform:/plugin/path, workspace://, file://
| description      | Display | Script description. Used as a tooltip on the script.
| toolbar          | IDE Integration | Bind a script to a view toolbar. Provide either view ID or view title (eg _Project Explorer_).
| menu             | IDE Integration | Bind a script to a view menu. Provide either view ID or view title (eg _Project Explorer_).
| popup            | IDE Integration | Bind a script to a popup menu. Set _value_ to an expression like enableFor(full.qualified.class.name). If the selection is an instance from that type or adaptable to it, the popup menu will be added.
| keyboard         | IDE Integration | Binds a script to a keyboard shortcut. Syntax for keyboard mappings is the same as in the 'Keys' preferences page.
| script-type      | Execution | Script type of this source. Typically the type is denoted from the file extension. This keyword explicitely sets the type.
| script-engine    | Execution | Comma separated list of engine IDs of valid engines for this script. Preceeding an engine with ! explicitely disables an engine ID.
| io               | Execution | Define where IO comes from/goes to: 'system' uses System.out/err/in; 'none' no IO at all; any other uses a console
| onStartup        | Event | Run a script on Eclipse/RCP startup. If an integer parameter is provided, it denotes the script startup delay in seconds.
| onShutdown       | Event | Run a script on Eclipse/RCP shutdown. If an integer parameter is provided, it denotes the maximum script execution time in seconds (defaults to 10s).
| onSave           | Event | Run a script on an editor save action. A wildcard pattern may be used to denote on which files a the script should trigger (eg: '*.js'). Parameter argv[0] holds the resource description of the saved file.
| onResourceChange | Event | Run a script on a resource change in the workspace. A wildcard pattern may be used to denote on which files a the script should trigger (eg: '*.js'). Parameter argv[0] holds the resource description of the changed file, argv[1] the type of the change.
| onEventBus       | Event | Run scripts when a specific event is posted on the OSGI message bus. Accepts a channel identifier for broker subscription (* wildcards may be used). The event will be passed to the script as variable 'event'.