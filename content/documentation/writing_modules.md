---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---

## Writing Modules

A module is a simple java class, providing some public methods. When a module gets loaded, EASE creates wrapper code in the target script language to access these public methods. Such methods are far more convenient to use than providing pure Java code. Furthermore they render your script more readable.

### Use existing java code

The simplest way to create such wrappers is to use the

    wrap(instance)
    
command, which is provided by the Environment module. Use it to wrap public methods from a given Java instance.

    homeFolder = new java.io.File("/home/me");
    wrap(homeFolder);
    exists(); // maps to homeFolder.exists()

While the file class might not be a suitable candidate for wrapping, utility classes are typically a better choice.

### Write your own module

Writing your own module gives you a better chance to fine tune which methods you like to expose to scripting. Start by creating a new extension point org.eclipse.ease.modules and add a module contribution to it. Provide a name and an implementing class, which may be a [POJO](https://en.wikipedia.org/wiki/Plain_old_Java_object). Optionally provide a category and the default visibility of your module. Modules may have dependencies to other modules (which will be loaded automatically on a _loadModule()_).

When implementing your class you may use _@WrapToScript_ annotations for methods (and static fields) to be exposed. Method parameters may be defined as optional in the script language. Therefore add a _@ScriptParameter(defaultValue="something")_ annotation in front of the parameter and provide a useful default value. As we may provide strings only, EASE will do its best to adapt the given string to the correct object type expected by the method.

A simple implementation might look like this

    public class Calculator {
    
        @WrapToScript
        public static final double PI = 3.1415;
    
        @WrapToScript
        public double area(double radius) {
            return radius * PI;
        }
    }

A more detailed example can be found in the [Code & me blog](http://codeandme.blogspot.com/2014/10/writing-modules-for-ease.html).

## Creating module documentation

Module documentation needs to be provided as eclipse help files in the same plugin as the module implementation. Creation of such files by hand is not recommended and also not documented. Instead EASE provides a doclet that can be used with the [javadoc tool](https://www.oracle.com/java/technologies/javase/javadoc.html) to create such files either manually or fully integrated within a tycho build.

### Prerequisites

The doclet is built nightly and can be [downloaded](https://ci.eclipse.org/ease/job/ease.build.nightly/lastSuccessfulBuild/artifact/ease.module.doclet.jar) from our [jenkins server](https://ci.eclipse.org/ease/).

Your class documentation needs to be well formed. Default javaDoc html files do accept XML errors like missing closing tags or unescaped HTML entities like '<' or '>' as part of the plain text. EASE does not, as it uses an XML parser to read these html files. So make sure your comments are clean. The doclet will run tests on the output to make sure we do not store invalid xml files.

Your extension point ID for the module needs to follow the naming scheme: <pluginId>.<moduleId_without_any_further_dots>. If this is not the case, linking between modules will not work correctly.

### Run the doclet manually

Run for Java 8 or older:

    <JDK8_location>\bin\javadoc.exe" -sourcepath <projectRoot>\src -root <projectRoot> -doclet org.eclipse.ease.helpgenerator.V8ModuleDoclet -docletpath ease.module.doclet.jar -failOnHTMLError true -failOnMissingDocs true -link https://docs.oracle.com/javase/8/docs/api <package_names_to_look_for_classes>

Run for Java 9 or newer:

    <JDK9_location>\bin\javadoc.exe" -sourcepath <projectRoot>\src -root <projectRoot> -doclet org.eclipse.ease.helpgenerator.V9ModuleDoclet -docletpath ease.module.doclet.jar -failOnHTMLError true -failOnMissingDocs true -link https://docs.oracle.com/javase/8/docs/api <package_names_to_look_for_classes>

### Run the doclet from maven

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
                <groupId>org.apache.maven.plugins</groupId>
                <version>${maven.javadoc.version}</version> 
                <configuration>
                    <outputDirectory>${project.build.directory}/../mydocs</outputDirectory>
                    <doclet>org.eclipse.ease.helpgenerator.V9ModuleDoclet</doclet>
                    <docletPath>${doclet.path}</docletPath>
                    <additionalparam>-root ${basedir}
                        -encoding 'UTF-8'
                        -failOnHTMLError true
                        -failOnMissingDocs true
                        -link
                            https://docs.oracle.com/en/java/javase/11/docs/api
                        -linkoffline
                            ../../org.eclipse.platform.doc.isv/reference/api/
                            http://help.eclipse.org/oxygen/topic/org.eclipse.platform.doc.isv/reference/api
                    </additionalparam>
                    <additionalOptions>-root ${basedir}
                        -encoding 'UTF-8'
                        -failOnHTMLError true
                        -failOnMissingDocs true
                        -link
                            https://docs.oracle.com/en/java/javase/11/docs/api
                        -linkoffline
                            ../../org.eclipse.platform.doc.isv/reference/api/
                            http://help.eclipse.org/oxygen/topic/org.eclipse.platform.doc.isv/reference/api
                        -linkoffline
                            ../../org.eclipse.ease.help/help/api-docs/javadoc/
                            https://ci.eclipse.org/ease/job/ease.build.core/JavaDoc
                    </additionalOptions>
                    <useStandardDocletOptions>false</useStandardDocletOptions>
                </configuration>
                <executions>
                    <execution>
                        <id>build-docs</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>javadoc</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
     
            <plugin>
                <artifactId>build-helper-maven-plugin</artifactId>
                <groupId>org.codehaus.mojo</groupId>
                <version>${maven.buildhelper.version}</version>
                <executions>
                    <execution>
                        <id>add_help</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>add-resource</goal>
                        </goals>
                        <configuration>
                            <resources>
                                <resource>
                                    <directory>${basedir}/help</directory>
                                    <targetPath>help</targetPath>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
    
_${doclet.path}_ needs to be set correctly to the location of the doclet.jar

Make sure to select the right doclet _V8ModuleDoclet_ or _V9ModuleDoclet_ depending on your java version.

* -failOnHTMLError: forces comments to be valid HTML code. This is required for inline help display as this is based on an XML parser which requires valid encoding
* -failOnMissingDocs: requires each module/method/parameter/return type/exception to be documented. Recommended as users expect full documentation on methods
* -link/-linkoffline: create documentation links to java classes. Same syntax as the standard java doclet provides.

### Documentation links

Links to classic javadoc files, eg {@link Collection#toString()} will automatically be resolved correctly. However the current doclet implementation will always create links for documenation created with java9 or lower. Form Java10 onwards the style of HTML pages changed, therefore links will not work correctly anymore.

You may also link to other module descriptions or methods within the same module. Therefore use

* {@module #methodName()}
* {@module #fieldName}
* {@module module.id.of.target.module#methodName()}

### Documenting examples

To provide examples of method calls you may use the @scriptExample annotation in comments:

    /**
     * @scriptExample thisIsYourCall("with", "some", "parameters") ... and this is the description
     * @scriptExample thisIsYourCall("with", "other", "parameters") ... some changed behavior
     */

This type of examples is useful for simple showcases of a function call. They do not work for complex or multiline statements. Best use '...' to separate code from description. If this token cannot be found we try to find the end of the function call by counting closing brackets.