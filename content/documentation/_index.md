---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---

## {{% fa icon-book %}} Documentation

{{% indent %}}[Getting started](getting_started)

{{% indent %}}[Basic Concept](concept)

{{% indent %}}[User Scripts](scripts)

{{% indent %}}[Supported Script Engines](supported_engines)

{{% indent %}}[Writing Script Modules](writing_modules)

## {{% fa icon-book %}} Blog Posts

{{% indent %}}[Trace Compass Scripting Engines Benchmarks](http://versatic.net/tracecompass/scriptEngineBenchmarks.html)

{{% indent %}}[Trace Compass Scripting: Empowering Users With Their Trace Data Analysis](http://versatic.net/tracecompass/introducingEase.html)

{{% indent %}}[Building UIs with EASE](http://codeandme.blogspot.com/search?q=ease&max-results=20&by-date=true)

{{% indent %}}[Launching with EASE](https://kichwacoders.com/2015/10/04/launching-with-ease/)

{{% indent %}}[Unit Testing with scripts in EASE](http://codeandme.blogspot.com/2015/05/unit-testing-with-scripts-in-ease.html)

{{% indent %}}[Live charting with EASE](http://codeandme.blogspot.com/2015/04/live-charting-with-ease.html)

{{% indent %}}[EASE scripts conquer the UI](http://codeandme.blogspot.com/2014/12/ease-scripts-conquer-ui.html)

{{% indent %}}[How to use Python to hack your Eclipse IDE](https://opensource.com/life/16/2/how-use-python-hack-your-ide)

{{% indent %}}[Eclipse Scripting Basics (EASE)](https://rage5474.github.io/2017/01/08/Eclipse-Scripting-Basics-EASE.html)

{{% indent %}}[A new script engine for EASE](http://codeandme.blogspot.com/p/blog-page_47.html)

{{% indent %}}[Writing modules for EASE](http://codeandme.blogspot.com/2014/10/writing-modules-for-ease.html)

{{% indent %}}[Eclipse EASE: Scripting für alle! (german)](https://jaxenter.de/eclipse-ease-scripting-fur-alle-891)

## {{% fa icon-book %}} Presentations

#### EclipseCon Europe 2018
{{% indent %}}[EASEy IDE extensions without Plugins](https://youtu.be/HfRMQ3oDs-g)  
{{% indent %}}[SysBox: An Eclipse Story](https://youtu.be/0B2sttEdHio)
#### EclipseCon Europe 2017
{{% indent %}}[And the people said, "Let there be scripts"](https://youtu.be/r7SSf5DMihQ)
#### EclipseCon France 2017
{{% indent %}}[How EASE unleashes the scientific power of Airbus' engineers in Eclipse](https://www.youtube.com/watch?v=4ZkV3MDxabc)
#### EclipseCon Europe 2016
{{% indent %}}[EASE-ily make the most of Eclipse with Python](https://youtu.be/bhhypyRAPp0)  
{{% indent %}}[Elevate your IDE with scripts](https://youtu.be/K9ERWs2ixZY)
#### EclipseCon France 2016
{{% indent %}}[Elevate your IDE with scripts](https://youtu.be/eWNKrPELcBw)
#### EclipseCon Europe 2015
{{% indent %}}[I love scripting](https://youtu.be/C15pwEBSgBA)
#### EclipseCon Europe 2013
{{% indent %}}[Scripting, baby!](https://youtu.be/FQw3eNYTgfI)
#### Further videos
{{% indent %}}[Scripting in Eclipse RCP-based applications](https://youtu.be/32TqigGZSGg)  
{{% indent %}}[Eclipse EASE](https://youtu.be/PDAnK0lkANg)  
{{% indent %}}[Easy scripting, easy modeling](https://youtu.be/mSjxwVRIBf0)