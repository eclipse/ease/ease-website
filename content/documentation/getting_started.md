---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---

## Getting Started

To get started, simply install EASE, switch to the _Scripting Perspective_ and enter script commands in the _Script Shell_ view.
You may check out this [video tutorial](https://www.youtube.com/watch?v=FQw3eNYTgfI) or try it by following these steps.

### Installing EASE

Use __Help / Install New Software...__ and install from our integration update site:

{{% indent %}}{{% fa fa-download %}} Latest: <span class="p2site" onclick="javascript:prompt('Hit Ctrl-c to copy', 'https://download.eclipse.org/ease/release/latest');">https://download.eclipse.org/ease/release/latest</span>

![Install Components](../../images/install_ease.png)

### Perspective & Script Shell

[Switch the perspective](https://help.eclipse.org/latest/index.jsp?topic=%2Forg.eclipse.platform.doc.user%2Ftasks%2Ftasks-9f.htm) to _Scripting_. The upper section provides a JavaScript shell with an input field at the lower end of the view. Enter your first command:

    print("Hello World");

access Java ...

    new java.io.File("/home/johnDoe/todoList.txt").exists();
    
or Eclipse ...

    var eclipseWorkspace = org.eclipse.core.resources.ResourcesPlugin.getWorkspace().getRoot();

### Modules Explorer

On the righthand side the __Modules Explorer__ view shows existing script libraries. To load one, simply DnD it to the __Script Shell__. Afterwards you may use all commands listed for the loaded module.

The context menu of modules and methods provides access to the help description of the selected component.

### Where to go from here

* browse our [example script repository](https://gitlab.eclipse.org/eclipse/ease/ease-scripts)
* check out [tutorial videos](..)

