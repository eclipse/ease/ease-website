---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---

## Scripting Support by EASE

EASE adds scripting functionalities to the Eclipse IDE. The main focus is on script execution within the context of the running IDE.

There already exist several Eclipse projects that aide on project management, provide editor support and allow to execute scripts. EASE now adds the component to run scripts in the context of the running IDE and the underlying JDK. This provides access to java classes, to Eclipse, the workspace, its views and commands. Some of the key areas where EASE can help are:

* automating tasks in the IDE
* provide simple DSLs for your users
* rapid prototyping of IDE elements
* dynamic behavior based on events

### Script Modules

While scripts may directly instantiate and access Java classes, this might be a quite cumbersome task for your users. EASE therefore allows to write simple Java libraries that will be added to the context of the selected script language. The idea is: write it once in Java, use it in every script language.

We call these libraries *modules* and allow to load them dynamically during script execution. Further there exists UI support for browsing modules, DnD support for methods and code completion (at least for [JSDT](https://projects.eclipse.org/projects/webtools.jsdt)). By [providing your own modules](../writing_modules) you can easily build up your own [DSL](https://en.wikipedia.org/wiki/Domain-specific_language) for your users.

### Extending the UI

Typically scripts are executed by using a run target. But By adding simple keywords to script comment headers, you may dynamically add a toolbar button or a menu entry to a view or the main toolbar, that triggers your script. This allows to augment the IDE with custom functionality in a simple way. You may even add new views that are purely scripted!

### Script Engine Sandboxing

EASE allows to run multiple script engines in parallel. Each of these engines runs in its own sandbox, not interfering with other engines. However a dedicated module allows scripts to exchange data with each other.

Engines may run in [REPL mode](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop), similar to a terminal or terminate after executing a script file. A special Eclipse application exists, to start eclipse headless, execute a script and terminate afterwards.