---
hide_breadcrumb: true
page_css_file: "/css/ease.css"
---

## Supported Script Engines

  Language   | Engine        | Debug Support | Details |
  |:--------:|:--------:|:--------:|:--------:|
  JavaScript | [Mozilla Rhino](https://github.com/mozilla/rhino) | {{% fa fa-check green %}} |  |
  JavaScript | [Nashorn](https://openjdk.org/projects/nashorn/)  | {{% fa fa-times red %}}   | Available from Java 8 - Java 14 as part of the JRE |
  Python     | [Py4J](https://www.py4j.org/)                     | {{% fa fa-check green %}} | Bridges JDK and an external Python interpreter |
  Python     | [Jython](https://www.jython.org/)                 | {{% fa fa-check green %}} | Supports Python up to Python 2.7 only. No 3.x support |
  Ruby       | [JRuby](https://www.jruby.org/)                   | {{% fa fa-times red %}}   | Proof of concept implementation. Not actively maintained. |
  Groovy     | [Groovy](https://groovy-lang.org/index.html)      | {{% fa fa-times red %}}   | Proof of concept implementation. Not actively maintained. |

### Meta Engines

We further support engines that actually do not really interpret script code, but use the same EASE scripting interface.

| Engine    | Purpose  |
|:---------:|:--------:|
| TestSuite | Executes *.suite files to run unit tests written in script languages. |
| Archive   | Runs scripts contained in a *.sar archive (similar to *.jar files). |
| JDK       | Runs *.java files from the current workspace. |
