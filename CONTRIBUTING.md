# Contributing to EASE website

Thanks for your interest in this project.

## Project description

This repository hosts the website for the [EASE framework](https://eclipse.dev/ease/).

## Developer resources

The project maintains the following source code repositories

* [https://gitlab.eclipse.org/eclipse/ease/ease-website](https://gitlab.eclipse.org/eclipse/ease/ease-website)

## Eclipse Contributor Agreement

Before your contribution can be accepted by the project team contributors must
electronically sign the Eclipse Contributor Agreement (ECA).

* [https://www.eclipse.org/legal/ECA.php](https://www.eclipse.org/legal/ECA.php)

Commits that are provided by non-committers must have a Signed-off-by field in
the footer indicating that the author is aware of the terms by which the
contribution has been provided to the project. The non-committer must
additionally have an Eclipse Foundation account and must have a signed Eclipse
Contributor Agreement (ECA) on file.

For more information, please see the Eclipse Committer Handbook:
[https://www.eclipse.org/projects/handbook/#resources-commit](https://www.eclipse.org/projects/handbook/#resources-commit)

## Contact

Contact the EASE development team on the [Developer Mailing List](https://dev.eclipse.org/mailman/listinfo/ease-dev).
